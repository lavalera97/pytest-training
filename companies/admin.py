from django.contrib import admin
from companies.models import Company


class CompanyAdmin(admin.ModelAdmin):

    list_display = ('name', 'application_link', 'notes', 'status')
    ordering = ('-last_update',)
    list_editable = ('status',)
    search_fields = ('name', 'application_link')


admin.site.register(Company, CompanyAdmin)


