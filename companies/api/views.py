from rest_framework.viewsets import ModelViewSet
from companies.models import Company
from .serializers import CompanySerializer
from rest_framework.pagination import PageNumberPagination


class CompanyViewSet(ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all().order_by('-last_update')
    pagination_class = PageNumberPagination

