from django.urls import path, include
from rest_framework.routers import DefaultRouter
from companies.api.views import CompanyViewSet

router = DefaultRouter()
router.register('company', viewset=CompanyViewSet, basename='companies')

urlpatterns = [
    path('', include(router.urls))
]

