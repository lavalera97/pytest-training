from django.db import models


class Company(models.Model):

    class CompanyStatus(models.TextChoices):
        LAYOFFS = 'Layoffs'
        HIRING_FREEZE = 'Hiring freeze'
        HIRING = 'Hiring'

    name = models.CharField(max_length=40, unique=True)
    status = models.CharField(choices=CompanyStatus.choices, default=CompanyStatus.HIRING, max_length=40)
    created_at = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    application_link = models.URLField(blank=True)
    notes = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'

    def __str__(self):
        return self.name
