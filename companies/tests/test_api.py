from django.urls import reverse
from rest_framework import status
from companies.models import Company
import pytest
import json
# from unittest import TestCase
# from django.test import Client


companies_url = reverse('companies-list')
pytestmark = pytest.mark.django_db

# ------------------TEST GET COMPANIES-----------------


def test_zero_companies_list(client):
    response = client.get(companies_url)
    assert response.status_code == status.HTTP_200_OK


def test_one_company_exists(client):
    amazon = Company.objects.create(name='Amazon')
    response = client.get(companies_url)
    response_content = json.loads(response.content)
    assert response_content[0]['name'] == 'Amazon'
    assert response.status_code == status.HTTP_200_OK
    amazon.delete()
    response = client.get(companies_url)
    assert response.data == []

# ------------------TEST POST COMPANIES-----------------


def test_create_company(client):
    data = {
        'name': 'Netflix'
    }
    response = client.post(reverse('companies-list'), data)
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data['name'] == 'Netflix'
    assert response.data['status'] == 'Hiring'


def test_empty_create_company(client):
    data = {}
    response = client.post(reverse('companies-list'), data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {'name': ['This field is required.']}


def test_create_existing_company_fail(client):
    Company.objects.create(name='Amazon')
    response = client.post(reverse('companies-list'), data={'name': 'Amazon'})
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_create_company_with_wrong_status(client):
    response = client.post(reverse('companies-list'), data={'name': 'Random name', 'status': 'WrongStatus'})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert 'WrongStatus' in str(response.data)


@pytest.mark.xfail
def test_ok_if_fails():
    assert 1 == 2


@pytest.mark.skip(reason='For testing purposes')
def test_should_be_skipped():
    assert 1 == 2


def raise_exception():
    raise ValueError('Corona exception')


def test_raise_exception_should_pass():
    with pytest.raises(ValueError) as e:
        raise_exception()
    assert 'Corona exception' == str(e.value)







# @pytest.mark.django_db
# class BasicCompanyTestCase(TestCase):
#     def setUp(self):
#         self.client = Client()
#
#     def tearDown(self):
#         pass
#
#
# class TestGetCompanies(BasicCompanyTestCase):
#
#     def test_zero_companies_list(self):
#         response = self.client.get(reverse('companies-list'))
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#
#     def test_one_company_exists(self):
#         amazon = Company.objects.create(name='Amazon')
#         response = self.client.get(reverse('companies-list'))
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         amazon.delete()
#         response = self.client.get(reverse('companies-list'))
#         self.assertEqual(response.data, [])
#
#
# class TestPostCompanies(BasicCompanyTestCase):
#
#     def test_create_company(self):
#         data = {
#             'name': 'Netflix'
#         }
#         response = self.client.post(reverse('companies-list'), data)
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)
#         self.assertEqual(response.data['name'], 'Netflix')
#         self.assertEqual(response.data['status'], 'Hiring')
#
#     def test_empty_create_company(self):
#         data = {}
#         response = self.client.post(reverse('companies-list'), data)
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#         self.assertEqual(response.data, {'name': ['This field is required.']})
#
#     def test_create_existing_company_fail(self):
#         Company.objects.create(name='Amazon')
#         response = self.client.post(reverse('companies-list'), data={'name': 'Amazon'})
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#
#     def test_create_company_with_strong_status(self):
#         response = self.client.post(reverse('companies-list'), data={'name': 'Random name', 'status': 'WrongStatus'})
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#         self.assertIn('WrongStatus', str(response.data))
#
#
# @pytest.mark.xfail
# def test_ok_if_fails():
#     assert 1 == 2
#
#
# @pytest.mark.skip(reason='For testing purposes')
# def test_should_be_skipped():
#     assert 1 == 2
#
#
# def raise_exception():
#     raise ValueError('Corona exception')
#
#
# def test_raise_exception_should_pass():
#     with pytest.raises(ValueError) as e:
#         raise_exception()
#     assert 'Corona exception' == str(e.value)