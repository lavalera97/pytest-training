from datetime import datetime
import pytest


@pytest.fixture
def time_tracker():
    time = datetime.now()
    yield
    time_end = datetime.now()
    diff = time - time_end
    print(f'\n runtime: {diff.total_seconds()}')
